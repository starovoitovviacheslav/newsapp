//
//  NewsTableViewCell.swift
//  NewsApp
//
//  Created by Slava Starovoitov on 27.09.2021.
//

import UIKit

class NewsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageTitle: UIImageView!
    @IBOutlet weak var sourceName: UILabel!
    @IBOutlet weak var date: UILabel!
    
    func setup(viewModel: NewsViewModel) {
        titleLabel.font = UIFont(name: "TiemposHeadline-Medium", size: 20)
        sourceName.font = UIFont(name: "TiemposHeadline-Medium", size: 14)
        date.font = UIFont(name: "TiemposHeadline-Medium", size: 14)
        
        titleLabel.text = viewModel.title
        descriptionLabel.text = viewModel.description
        imageTitle.load(urlString: viewModel.urlToImage)
        sourceName.text = viewModel.sourceName
        date.text = viewModel.date.dateFormating()
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.titleLabel.text = nil
        self.descriptionLabel.text = nil
        self.imageTitle.image = nil
        self.sourceName.text = nil
        self.date.text = nil
        
    }
    
}

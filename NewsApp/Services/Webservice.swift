//
//  Webservice.swift
//  NewsApp
//
//  Created by Slava Starovoitov on 26.09.2021.
//

import Foundation

class Webservice {
    
    static var shared = Webservice()
    
    private static var apiKey = "aebe78d6d7424e2a8ffbab4bf65384d3"
    
    private struct URLS {
        static let topHeadlines = "https://newsapi.org/v2/top-headlines?country=us&apiKey=\(Webservice.apiKey)"
        static let searchBar = "https://newsapi.org/v2/everything?sortedBy=popularity&apiKey=\(Webservice.apiKey)&q="
    }
    
    public func getArticles(completion: @escaping (([Article]?) -> Void)) {
        
        URLSession.shared.dataTask(with: URL(string: URLS.topHeadlines)!) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                completion(nil)
            } else if let data = data {
                
                do {
                    let news = try JSONDecoder().decode(News.self, from: data)
                    completion(news.articles)
                    print(news)
                } catch {
                    print(String(describing: error))
                }
                
                print(data)
            }
        }.resume()
        
    }
    
    public func search(with query: String, completion: @escaping (([Article]?) -> Void)) {
        guard !query.trimmingCharacters(in: .whitespaces).isEmpty else {
            return
        }
        
        let urlString = URLS.searchBar + query
        
        guard let url = URL(string: urlString) else { return }
        
        print(url)
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                completion(nil)
            } else if let data = data {
                
                do {
                    let news = try JSONDecoder().decode(News.self, from: data)
                    completion(news.articles)
                    print(news)
                } catch {
                    print(String(describing: error))
                }
                
                print(data)
            }
        }.resume()
        
    }
    
}

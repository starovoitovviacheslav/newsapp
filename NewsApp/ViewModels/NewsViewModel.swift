//
//  NewsViewModel.swift
//  NewsApp
//
//  Created by Slava Starovoitov on 26.09.2021.
//

import Foundation

struct NewsListViewModel {
    let news: [Article]
}

extension NewsListViewModel {
    
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return self.news.count
    }
    
    func articleAtIndex(_ index: Int) -> NewsViewModel {
        let article = self.news[index]
        return NewsViewModel(article)
    }
    
}

struct NewsViewModel {
    private let article: Article
}

extension NewsViewModel {
    init(_ article: Article) {
        self.article = article
    }
}

extension NewsViewModel {
    var title: String {
        return self.article.title ?? ""
    }
    var description: String {
        return self.article.description ?? ""
    }
    var urlToImage: String {
        return self.article.urlToImage ?? ""
    }
    var sourceName: String {
        return self.article.source?.name ?? ""
    }
    var date: String {
        return self.article.publishedAt ?? ""
    }
    var url: String {
        return self.article.url ?? ""
    }
}

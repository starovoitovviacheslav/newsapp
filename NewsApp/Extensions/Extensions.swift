//
//  Extencions.swift
//  NewsApp
//
//  Created by Slava Starovoitov on 29.09.2021.
//

import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()

//MARK: - Navigation Controller
extension UINavigationController {
    func custom() {
        self.view.backgroundColor = UIColor(red: 255/255, green: 248/255, blue: 238/255, alpha: 1)
        self.navigationBar.prefersLargeTitles = true
        self.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black, NSAttributedString.Key.font: UIFont(name: "TiemposHeadline-Medium", size: 40)!]
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black, NSAttributedString.Key.font: UIFont(name: "TiemposHeadline-Medium", size: 20)!]
    }
}

//MARK: - UIImage
extension UIImageView {
    
    func load(urlString: String) {
        if let image = imageCache.object(forKey: urlString as AnyObject) {
            self.image = image as? UIImage
            return
        }
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        imageCache.setObject(image, forKey: urlString as NSString)
                        self?.image = image
                    }
                }
            }
        }
        
    }
}

//MARK: - String
extension String {
    func dateFormating() -> String? {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssXXXXX"
        if let date = formatter.date(from: self) {
            formatter.dateFormat = "MMM d, h:mm a"
            let string = formatter.string(from: date)
            return string
        }
        return nil
    }
}

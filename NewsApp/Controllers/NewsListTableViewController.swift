//
//  NewsListTableViewController.swift
//  NewsApp
//
//  Created by Slava Starovoitov on 26.09.2021.
//

import UIKit

class NewsListTableViewController: UITableViewController, UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    private var newsListViewModel: NewsListViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        
        overrideUserInterfaceStyle = .light
        self.navigationController?.custom()
        
        Webservice.shared.getArticles { news in
            if let news = news {
                self.newsListViewModel = NewsListViewModel(news: news)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
        
    }
    
    
    //MARK: - TableView Delegates
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.newsListViewModel == nil ? 0 : self.newsListViewModel.numberOfSections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.newsListViewModel.numberOfRowsInSection(section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsTableViewCell", for: indexPath) as? NewsTableViewCell else { fatalError("cell can't cast") }
        
        let newsVM = self.newsListViewModel.articleAtIndex(indexPath.row)
        cell.setup(viewModel: newsVM)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextVC = DetailViewController()
        self.navigationController?.pushViewController(nextVC, animated: true)
        nextVC.articleUrl = newsListViewModel.articleAtIndex(indexPath.row).url
    }
    
    //MARK: - SearchBar Delegates
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text, !text.isEmpty else {
            return
        }

        Webservice.shared.search(with: text) { news in
            if let news = news {
                self.newsListViewModel = NewsListViewModel(news: news)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
}



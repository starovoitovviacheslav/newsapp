//
//  DetailViewController.swift
//  NewsApp
//
//  Created by Slava Starovoitov on 30.09.2021.
//

import UIKit
import WebKit

class DetailViewController: UIViewController {
    
    let webView = WKWebView()
    
    var articleUrl = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(webView)
        
        guard let url = URL(string: articleUrl) else { return }
        
        webView.load(URLRequest(url: url))
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        webView.frame = view.bounds
    }
    
}
